# Projekt PDS - Multifunkční senzor


## Zadání

* Vytvoreni prototypu hw prvku a demonstrace v aplikaci HomeAssistant.
* ESP bude notifikovat o pohybu na zaklade informace z pohyboveho senzoru.
* Inspirace https://github.com/bruhautomation/ESP-MQTT-JSON-Multisensor
* Komunikace MQTT.
* MQTT broker soucasti HASS.IO

## Řešení

Projekt byl řešen za pomocí vývojové plaformy ESP32, na níž byl připojen multisenzoz na měření teploty a vlhkosti a pohybové čidlo. ESP32 obstarává sběr dat ze senzorů a následně zajistí přenos po síti na Raspberry Pi, na kterém běží systém pro automatizaci HASS.IO.

ESP32 v případě prvního spuštění, nebo pokud v okolí není známá wifi síť, vytvoří hotspot a po připojení na něj je možné vybrat z viditelných sítí, na kterou se má připojit.

## Komponenty a zapojení

Seznam potřebných komponent:
* ESP32
* senzor DHT22
* IR pohybové čídlo
* Raspberri Pi

### DHT22
* VCC na 3,3V na ESP32
* GND na zem na ESP32
* DATA na pin IO14

Mezi napájecí a datový vodič je paralelně k senzoru připojen odpor 10 kilo ohmů.

### IR sensor
* VCC na 3,3V na ESP32
* GND na zem na ESP32
* DATA na pin IO27

### Raspberri Pi
Bylo pottřeba nainstalovat Hassio, následně provest konfiguraci v souboru /config/configuration.yaml:
```
homeassistant:
  # Name of the location where Home Assistant is running
  name: Home
  # Location required to calculate the time the sun rises and sets
  latitude: 49.2
  longitude: 16.6333
  # Impacts weather/sunrise data (altitude above sea level in meters)
  elevation: 202
  # metric for Metric, imperial for Imperial
  unit_system: metric
  # Pick yours from here: http://en.wikipedia.org/wiki/List_of_tz_database_time_zones
  time_zone: Europe/Prague
  # Customization file
  customize: !include customize.yaml


# Enables the frontend
frontend:

# Enables configuration UI
config:

http:
  # Secrets are defined in the file secrets.yaml
  # api_password: !secret http_password
  # Uncomment this if you are using SSL/TLS, running in Docker container, etc.
  # base_url: example.duckdns.org:8123

# Checks for available updates
# Note: This component will send some information about your system to
# the developers to assist with development of Home Assistant.
# For more information, please see:
# https://home-assistant.io/blog/2016/10/25/explaining-the-updater/
updater:
  # Optional, allows Home Assistant developers to focus on popular components.
  # include_used_components: true


# Discover some devices automatically
discovery:

# Allows you to issue voice commands from the frontend in enabled browsers
conversation:

# Enables support for tracking state changes over time
history:

# View all events in a logbook
logbook:

# Enables a map showing the location of tracked devices
map:

mqtt:
  broker: 127.0.0.1

# Weather prediction
sensor:
  - platform: mqtt
    state_topic: 'PDS/multisensor'
    name: 'Temperature'
    unit_of_measurement: '°C'
    value_template: '{{ value_json.temperature }}'
  - platform: mqtt
    state_topic: 'PDS/multisensor'
    name: 'Humidity'
    unit_of_measurement: '%'
    value_template: '{{ value_json.humidity }}'
  - platform: mqtt
    state_topic: 'PDS/multisensor'
    name: 'Motion'
    unit_of_measurement: ''
    value_template: '{{ value_json.motion }}'
# Text to speech
tts:
  - platform: google

# Cloud
cloud:

group: !include groups.yaml
automation: !include automations.yaml
script: !include scripts.yaml

panel_iframe:
  configurator:
    title: Configurator
    icon: mdi:wrench
    url: http://hassio.local:3218

```

## MQTT komunikace

Pro komunikaci je využito zprávy v jednotném formátu: 
```
{
  "temperature": "24.30",
  "humidity": "53.60",
  "motion": "false"
}
```
Zpráva je zasílána na topic **PDS/multisensor** a je zasílána ve dvou případech: periodicky každých 10 vteřin a v případě detekování pohybu  IR pohybovým čidlem.
