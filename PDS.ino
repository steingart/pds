#if defined(ESP8266)
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#else
#include <WiFi.h>          //https://github.com/esp8266/Arduino
#endif

//needed for library
#include <DNSServer.h>
#if defined(ESP8266)
#include <ESP8266WebServer.h>
#else
#include <WebServer.h>
#endif
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager

#include <PubSubClient.h>
#include <SimpleDHT.h>
#include <ArduinoJson.h>

SimpleDHT22 dht22;
WiFiClient espClient;
PubSubClient mqttClient(espClient);

#define MQTT_VERSION MQTT_VERSION_3_1_1
#define  PIN_DHT22 14
#define  PIN_PIR 27

const char* mqttServer = "192.168.1.178";
const int mqttPort = 1883;
const PROGMEM char* MQTT_SENSOR_TOPIC = "PDS/multisensor";


volatile int interruptCounter;
hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

char msg[50];
 float temperature = 0;
  float humidity = 0;
  int pirStatus;
int pirValue;
String motionStatus;


void onTimer() {
  portENTER_CRITICAL_ISR(&timerMux);
  interruptCounter++;
  portEXIT_CRITICAL_ISR(&timerMux);
 
}

void setup() {
    Serial.begin(115200);

    pinMode(PIN_PIR, INPUT);


    //WiFiManager
    WiFiManager wifiManager;
   
    wifiManager.autoConnect("AutoConnectAP");
   
    Serial.println("connected...yeey :)");

    mqttClient.setServer(mqttServer, mqttPort);
 
    

timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, 10000000, true); //60sec
  timerAlarmEnable(timer);
}

void publishData() {

while (!mqttClient.connected()) {
    Serial.println("Connecting to MQTT...");
 
    if (mqttClient.connect("ESP32Client")) {
 
      Serial.println("connected");  
 
    } else {
 
      Serial.print("failed with state ");
      Serial.print(mqttClient.state());
      delay(2000);
 
    }
}
    
  
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  // INFO: the data must be converted into a string; a problem occurs when using floats...
  root["temperature"] = (String)temperature;
  root["humidity"] = (String)humidity;
  root["motion"] = (String)motionStatus;

  root.prettyPrintTo(Serial);
  Serial.println("");
 
  char data[200];
  root.printTo(data, root.measureLength() + 1);
  mqttClient.publish(MQTT_SENSOR_TOPIC, data, true);
  yield();
}

void loop() {

 pirValue = digitalRead(PIN_PIR); //read state of the

    if (pirValue == LOW && pirStatus != 1) {
      motionStatus = "false";
      publishData();
      pirStatus = 1;
    }
    else if (pirValue == HIGH && pirStatus != 2) {
      motionStatus = "true";
      publishData();
      pirStatus = 2;
    }
    
if (interruptCounter > 0) {
 
    portENTER_CRITICAL(&timerMux);
    interruptCounter--;
    portEXIT_CRITICAL(&timerMux);

int err = SimpleDHTErrSuccess;

    if ((err = dht22.read2(PIN_DHT22, &temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT22 failed, err="); Serial.println(err);
    
  }
  else{
    publishData();  
  }
  } 



}
